<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'App\Http\Controllers\AuthController@login');
    Route::post('signup', 'App\Http\Controllers\AuthController@signup');
    Route::post('checkEmail', 'App\Http\Controllers\AuthController@checkEmail');
    Route::post('getCode', 'App\Http\Controllers\AuthController@getCode');
    Route::post('confirm', 'App\Http\Controllers\AuthController@confirm');
    Route::post('setPassword', 'App\Http\Controllers\AuthController@setPassword');


    Route::group([
        'middleware' => 'auth:api'
    ], function() {

//clients
        Route::post('security_add_client', 'App\Http\Controllers\ClientController@security_add_client');
        Route::get('security_get_client', 'App\Http\Controllers\ClientController@security_get_client');
        Route::get('security_get_client_detail/{id}', 'App\Http\Controllers\ClientController@security_get_client_detail');
        Route::post('security_update_client_detail/{id}', 'App\Http\Controllers\ClientController@security_update_client_detail');
        Route::post('admin_get_clients', 'App\Http\Controllers\ClientController@admin_get_clients');

        //Branches
        Route::post('security_add_branch/{id}', 'App\Http\Controllers\BranchController@security_add_branch');
        Route::get('security_get_branch/{id}', 'App\Http\Controllers\BranchController@security_get_branch');
        Route::get('security_get_branch_detail/{id}', 'App\Http\Controllers\BranchController@security_get_branch_detail');
        Route::post('security_update_branch_detail/{id}', 'App\Http\Controllers\BranchController@security_update_branch_detail');
        Route::post('branch_get_report', 'App\Http\Controllers\BranchController@branch_get_report');
        Route::post('security_add_contact/{id}', 'App\Http\Controllers\BranchController@security_add_contact');
        Route::post('admin_get_branches', 'App\Http\Controllers\BranchController@admin_get_branches');

        //Guards
        Route::post('security_add_guard/{id}', 'App\Http\Controllers\GuardController@security_add_guard');
        Route::post('security_get_guards/{id}', 'App\Http\Controllers\GuardController@security_get_guards');
        Route::post('security_edit_guards/{id}', 'App\Http\Controllers\GuardController@security_edit_guards');
        Route::post('guard_get_report', 'App\Http\Controllers\GuardController@guard_get_report');
        Route::get('branch_get_guards', 'App\Http\Controllers\GuardController@branch_get_guards');
        Route::post('admin_get_guards', 'App\Http\Controllers\GuardController@admin_get_guards');

        //Department
        Route::post('client_add_department', 'App\Http\Controllers\DepartmentController@client_add_department');
        Route::post('client_get_department', 'App\Http\Controllers\DepartmentController@client_get_department');
        Route::post('client_edit_department', 'App\Http\Controllers\DepartmentController@client_edit_department');


        //IPRS
        Route::post('callIPRS', 'App\Http\Controllers\DataController@callIPRS');
        Route::post('client_save_data', 'App\Http\Controllers\DataController@client_save_data');
        Route::post('pushImage', 'App\Http\Controllers\DataController@pushImage');
        Route::post('searchOut', 'App\Http\Controllers\DataController@searchOut');
        Route::post('checkout', 'App\Http\Controllers\DataController@checkout');


        //DashBiard
        Route::get('guard_get_data', 'App\Http\Controllers\ApiController@guard_get_data');
        Route::get('security_get_data', 'App\Http\Controllers\ApiController@security_get_data');
        Route::get('client_get_data', 'App\Http\Controllers\ApiController@client_get_data');
        Route::get('admin_get_data', 'App\Http\Controllers\ApiController@admin_get_data');

        //profile
        Route::post('changePassword', 'App\Http\Controllers\AuthController@changePassword');
        Route::get('user', 'App\Http\Controllers\AuthController@user');

        //report
        Route::post('security_get_report', 'App\Http\Controllers\ReportController@security_get_report');
        Route::post('branch_get_report_by_guard', 'App\Http\Controllers\ReportController@branch_get_report_by_guard');
        Route::post('branch_get_report_by_dep', 'App\Http\Controllers\ReportController@branch_get_report_by_dep');
        Route::post('branch_get_report_by_frequent', 'App\Http\Controllers\ReportController@branch_get_report_by_frequent');
        Route::post('branch_get_report_by_rand', 'App\Http\Controllers\ReportController@branch_get_report_by_rand');
        Route::post('admin_get_report', 'App\Http\Controllers\ReportController@admin_get_report');
        Route::post('admin_get_report', 'App\Http\Controllers\ReportController@admin_get_report');
        Route::post('branch_get_report_by_range', 'App\Http\Controllers\ReportController@branch_get_report_by_range');

        //Users
        Route::get('security_get_users', 'App\Http\Controllers\UserController@security_get_users');
        Route::get('branch_get_users', 'App\Http\Controllers\UserController@branch_get_users');
        Route::post('add_user', 'App\Http\Controllers\UserController@add_user');
        Route::post('edit_user', 'App\Http\Controllers\UserController@edit_user');
        Route::get('admin_get_users', 'App\Http\Controllers\UserController@admin_get_users');

        //companies
        Route::get('admin_get_companies', 'App\Http\Controllers\CompanyController@admin_get_companies');
        Route::get('admin_get_company_detail/{id}', 'App\Http\Controllers\CompanyController@admin_get_company_detail');
        Route::post('admin_update_company_detail/{id}', 'App\Http\Controllers\CompanyController@admin_update_company_detail');
        Route::post('updateLogo', 'App\Http\Controllers\CompanyController@updateLogo');


//        RATTING
        Route::post('blacklist', 'App\Http\Controllers\RatingController@blacklist');
        Route::get('client_get_blacklist', 'App\Http\Controllers\RatingController@client_get_blacklist');
        Route::post('unlock', 'App\Http\Controllers\RatingController@unlock');


        //EPASS
        Route::get('client_get_epass', 'App\Http\Controllers\EpassController@client_get_epass');
        Route::post('client_get_epass_ontype', 'App\Http\Controllers\EpassController@client_get_epass_ontype');
        Route::post('client_get_epass_info', 'App\Http\Controllers\EpassController@client_get_epass_info');
        Route::post('client_get_epass_checkout', 'App\Http\Controllers\EpassController@client_get_epass_checkout');


        Route::get('security_get_all_locations', 'App\Http\Controllers\LocationController@security_get_all_locations');





    });
});
