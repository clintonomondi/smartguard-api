<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    use HasFactory;

    protected  $fillable=['name','contact','address','location','created_by','updated_by','status','client_id','sms'];
}
