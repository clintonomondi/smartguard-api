<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    use HasFactory;

    protected $fillable=['fname','mname','lname','id_no','kra','gender','dob','phone','temperature','department_id','car_reg_no','reason','client_id',
        'branch_id','created_by','updated_by','company_id','laptop','timein','timeout','status'];
}
