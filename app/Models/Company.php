<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected  $fillable=['company_name','country','address','contact','status','amount','pin','enabled','url'];
}
