<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Epass extends Model
{
    use HasFactory;

    protected  $fillable=['epass','data_id','served_by','epass_status','id_no','branch_id'];
}
