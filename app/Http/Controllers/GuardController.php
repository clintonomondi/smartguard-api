<?php

namespace App\Http\Controllers;

use App\Models\Data;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Knox\AFT\AFT;

class GuardController extends Controller
{
    public  function security_add_guard(Request $request,$id){
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'id_no' => 'required',
            'staff_no' => 'required',
        ]);
        $code = mt_rand(1000, 9999);
        $request['company_id']=Auth::user()->company_id;
        $request['client_id']=$id;
        $request['updated_by']=Auth::user()->id;
        $request['created_by']=Auth::user()->id;
        $request['password']=bcrypt($code);
        $request['user_type']='security_firm';
        $request['role']='guard';
        $request['status']='Active';
        $user=User::create($request->all());

        $phone=str_replace(' ','','0'.substr($request->phone,4));
        try{
            $message='Hello '.$request->name.',you have been on boarded on SmartGuard platform. Please use '.$code.' as your initial password. https://smatguard.net/login';
            AFT::sendMessage($phone, $message,'Postman');
        } catch (\Exception $e) {

        }


        return ['status'=>true,'message'=>'Guard created successfully, an email and sms sent to the guard successfully'];
    }

    public  function security_get_guards(Request $request,$id){
        $guards=User::where('role','guard')->where('branch_id',$request->branch_id)->where('client_id',$id)->get();
        return ['guards'=>$guards];
    }


    public  function branch_get_guards(){
        $guards=User::where('role','guard')->where('branch_id',Auth::user()->branch_id)->get();
        return ['guards'=>$guards];
    }

    public  function security_edit_guards(Request $request,$id){
        $user=User::find($id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'User updated successfully'];
    }

    public  function guard_get_report(Request $request){
        $id=Auth::user()->id;
        $report=DB::select( DB::raw("SELECT *,
(SELECT NAME FROM `departments` B WHERE B.id=A.department_id)department
 FROM `data` A WHERE created_by='$id' AND  DATE(created_at)='$request->date'") );
        return ['report'=>$report];
    }


    public  function admin_get_guards(Request $request){
        $guards=DB::select( DB::raw("SELECT *,
(SELECT NAME FROM branches B WHERE B.id=A.branch_id)branch
 FROM `users` A WHERE role='guard' AND client_id='$request->client_id'") );
        return ['guards'=>$guards];
    }
}
