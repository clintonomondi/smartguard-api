<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    public  function admin_get_companies(){
        $companies = DB::select( DB::raw("SELECT *,
(SELECT COUNT(*)vcount FROM clients B WHERE B.company_id=A.id)clients,
(SELECT COUNT(*) FROM branches B WHERE B.client_id IN (SELECT id FROM clients B WHERE B.company_id=A.id))branches,
(SELECT COUNT(*) FROM s_m_s B WHERE STATUS='Unpaid' AND B.client_id IN (SELECT id FROM clients B WHERE B.company_id=A.id))sms
 FROM `companies` A order by id desc") );

        return ['companies'=>$companies];
    }

    
    public  function admin_get_company_detail($id){
        $company=Company::find($id);
        $contacts=User::where('company_id',$id)->where('role','!=','guard')->get();
        return ['company'=>$company,'contacts'=>$contacts];
    }

    public  function admin_update_company_detail(Request $request,$id){
        $company=Company::find($id);
        $company->update($request->all());
        return ['status'=>true,'message'=>'information updated successfully'];
    }

    public function updateLogo(Request $request){
        $company=Company::find(Auth::user()->company_id);
        if(!empty($request['file'])) {
                $base64File=$request->file;
                $data = substr($base64File, strpos($base64File, ',') + 1);
                $file = base64_decode($data);
                $fileNameToStore = mt_rand(10000, 99999) . '_' . time() . '.' . 'jpg';
                Storage::disk('public')->put('Logos/' . $fileNameToStore, $file);
                Storage::delete('/public/Logos/'.$company->url); 
                $request['url'] = $fileNameToStore; 
                $company->update($request->all());

                return ['status'=>true,'message'=>'Logo updated successfully','url'=>$request->url];
        }else{
            return ['status'=>false,'message'=>'Please select a file'];
        }
    }
}
