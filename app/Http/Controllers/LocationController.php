<?php

namespace App\Http\Controllers;

use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{
    public  function security_get_all_locations(){
        $company_id=Auth::user()->company_id;
        $records = DB::select( DB::raw("SELECT name,phone,role,id,
(SELECT lat FROM locations B WHERE B.user_id=A.id)lat,
(SELECT lng FROM locations B WHERE B.user_id=A.id)lng,
(SELECT name FROM clients B WHERE B.id=A.client_id)client,
(SELECT name FROM branches B WHERE B.id=A.branch_id)branch
 FROM users A WHERE  company_id='$company_id'") );



        $data=[];
        foreach ( $records as $rec){
            if (empty($rec->lat) || empty($rec->lng)){
                $lat=-1.2.$rec->id;
                $lng=36.8.$rec->id;
            }else{
                $lat=$rec->lat;
                $lng=$rec->lng;
            }
            if($rec->role==='guard'){
                $icon='http://maps.google.com/mapfiles/ms/icons/red-dot.png';
            }else{
                $icon='http://maps.google.com/mapfiles/ms/icons/green-dot.png';
            }
            $array=array(
                "position"=>array(
                    "lat"=>floatval($lat),
                    "lng"=>floatval($lng),
                    "phone"=>$rec->phone,
                    "client"=>$rec->client,
                    "branch"=>$rec->branch,
                    "name"=>$rec->name,
                    "icon"=>$icon
                )
            );
            array_push($data, $array);
        }


        return ['position'=>$data];
    }
}
