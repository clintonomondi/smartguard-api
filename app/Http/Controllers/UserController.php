<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Knox\AFT\AFT;

class UserController extends Controller
{
    public  function security_get_users(){
        $users=User::where('company_id',Auth::user()->company_id)->where('role','!=','guard')->get();
        return ['users'=>$users];
    }

    public  function branch_get_users(){
        $users=User::where('branch_id',Auth::user()->branch_id)->where('role','!=','guard')->get();
        return ['users'=>$users];
    }

    public  function add_user(Request $request){

        $code = mt_rand(1000, 9999);
        $request['client_id']=Auth::user()->client_id;
        $request['company_id']=Auth::user()->company_id;
        $request['branch_id']=Auth::user()->branch_id;
        $request['user_type']=Auth::user()->user_type;
        $request['role']=Auth::user()->role;
        $request['password']=bcrypt($code);
        $request['status']='Active';
        $user=User::create($request->all());

        $phone=str_replace(' ','','0'.substr($request->phone,4));

        try{
            $message='Hello '.$request->name.',you have been on boarded on SmartGuard platform .Please use '.$code.' as your initial password. https://smatguards.net';
            AFT::sendMessage($phone, $message,'Postman');
        } catch (\Exception $e) {

        }
       return ['status'=>true,'message'=>'User added successfully'];
    }

    public  function edit_user(Request $request){
        $user=User::find($request->user_id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'User updated successfully'];
    }

    public  function admin_get_users(){
        $users=User::where('user_type','system_admin')->get();
        return ['users'=>$users];
    }
}
