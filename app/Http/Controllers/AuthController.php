<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Client;
use App\Models\Company;
use App\Models\OTP;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Knox\AFT\AFT;
use Illuminate\Support\Facades\Storage;

class AuthController extends Controller
{

    public function signup(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'phone' => 'required',
            'name' => 'required',
            'company_name' => 'required',
            'address' => 'required',
            'contact' => 'required',
            'country' => 'required',

        ]);

        $credentials = request(['email', 'password']);
        $email=User::where('email',$request->email)->count();
        if($email>0){
            return ['status'=>false,'message'=>'Email is already in use'];
        }
        if(!empty($request['file'])) {
            $base64File=$request->file;
            $data = substr($base64File, strpos($base64File, ',') + 1);
            $file = base64_decode($data);
            $fileNameToStore = mt_rand(10000, 99999) . '_' . time() . '.' . 'jpg';
            Storage::disk('public')->put('Logos/' . $fileNameToStore, $file);
            $request['url'] = $fileNameToStore;
        }
        $company=Company::create($request->all());
        $request['password']=bcrypt($request->password);
        $request['company_id']=$company->id;
        $request['branch_id']='NA';
        $request['client_id']='NA';
        $request['role']='admin';
        $request['user_type']='security_firm';
        $request['password_changed']='Yes';
        $user=User::create($request->all());
        if(!Auth::attempt($credentials)) {
            return ['status' => false, 'message' => 'We are not able to log you in, please use login page'];
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addHours(4);
        $token->save();
        $profile=User::find(Auth::user()->id);

        return ['status'=>true,'user'=>$profile,'company'=>$company,'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()

        ];


    }


    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return ['status'=>false,'message' => 'Invalid email or password'];

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addHours(4);
        $token->save();
        $profile=User::find(Auth::user()->id);
        $company=Company::where('id',Auth::user()->company_id)->first();
        if(empty($company)){
            $client=Client::find(Auth::user()->client_id);
            if(!empty($client)){
            $company=Company::where('id',$client->company_id)->first();
            }
        }

        $status = DB::select( DB::raw("SELECT STATUS AS user_status,
(SELECT STATUS FROM branches B WHERE B.id=A.branch_id)branch_status,
(SELECT STATUS FROM clients B WHERE B.id=A.client_id)client_status,
(SELECT STATUS FROM companies B WHERE B.id=A.company_id)company_status
  FROM `users` A  WHERE id='$profile->id'") );

        if(!empty($request->lat && !empty($request->lng))){
            $l = DB::select( DB::raw("INSERT INTO locations (user_id,lat,lng) VALUES('$profile->id','$request->lat','$request->lng') ON DUPLICATE KEY UPDATE lat=$request->lat,lng=$request->lng") );
        }
        if($status[0]->user_status=='Inactive' || $status[0]->branch_status=='Inactive' || $status[0]->client_status=='Inactive' || $status[0]->company_status=='Inactive'){
            return ['status'=>false,'message' => 'Unauthorised access, please contact support'];
        }


        return ['status'=>true,'user'=>$profile,'company'=>$company,'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()

        ];
    }

    public  function checkEmail(Request $request){
        $count=User::where('email',$request->email)->count();
        if($count>0){
            return ['status'=>false,'message'=>'Email already in use'];
        }else{
            return ['status'=>true,'message'=>'Success'];
        }
    }

    public  function getCode(Request $request){

        $check=User::where('email',$request->email)->count();
        $user=User::where('email',$request->email)->first();
        if($check<=0){
            return ['status'=>false,'message'=>'The email address does not exist'];
        }
         $request['phone']=$user->phone;
        $randomid = mt_rand(1000,9999);
        $request['code']=$randomid;
        $data=OTP::create($request->all());


        if(strlen($user->phone)==10){
            $phone=$user->phone;
        }else{
            $phone=str_replace(' ','','0'.substr($user->phone,4));
        }

        try{
            $message='Your four digit  verification code is '.$randomid;
            AFT::sendMessage($phone, $message,'Postman');
        } catch (\Exception $e) {

        }

        return ['status'=>true,'user'=>$user,'message'=>'A six digit verification code has been sent,enter the code'];

    }

    public function confirm(Request $request){
        $datas = DB::select( DB::raw("SELECT * FROM `o_t_p_s` WHERE phone='$request->phone' AND code='$request->code' AND created_at > NOW() - INTERVAL 4 HOUR") );
        if($datas==null){
            return ['status'=>false,'message'=>'Invalid code'];
        }
        return ['status'=>true,'message'=>'Success'];
    }

    public  function setPassword(Request $request){
        $user=User::find($request->id);
        if($request->password!=$request->repass){
            return ['status'=>false,'message'=>'Password do not match'];
        }
        $request['password']=bcrypt($request->password);
        $request['password_changed']='Yes';
        $user->update($request->all());
        return ['status'=>true,'message'=>'Password changed successfully, please login'];
    }

    public  function changePassword(Request $request){
        if($request->password!=$request->repass){
            return ['status'=>false,'message'=>'Password do not match'];
        }

        $currentpass = auth()->user()->password;
        if (!Hash::check($request['currentpass'], $currentpass)) {
            return ['status'=>false,'message'=>'The current password is invalid'];
        }
        $request['password']=bcrypt($request->password);
        $request['password_changed']='Yes';
        $user=User::find(Auth::user()->id);
        $user->update($request->all());

        $phone=str_replace(' ','','0'.substr($user->phone,4));
        try{
            $message='Your password has been successfully changed.If this was not you please contact SmartGuard Support center.';
            AFT::sendMessage($phone, $message,'Postman');
        } catch (\Exception $e) {

        }
        return ['status'=>true,'message'=>'Password successfully set, you will now use your new password to login'];
    }

    public  function user(){
       $id=Auth::user()->id;
        $user = DB::select( DB::raw("SELECT name,status,email,phone,role,
(SELECT company_name FROM companies B WHERE B.id=A.company_id)company_name,
(SELECT NAME FROM clients B WHERE B.id=A.client_id)client_name,
(SELECT NAME FROM branches B WHERE B.id=A.branch_id)branch,
(SELECT count(*)vcount FROM `data` WHERE created_by='$id' AND created_at>=CURDATE())records
 FROM users A WHERE id='$id'") );
        return ['user'=>$user,'user2'=>$user[0]];
    }


}
