<?php

namespace App\Http\Controllers;

use App\Models\Blacklist;
use App\Models\Branch;
use App\Models\Client;
use App\Models\Company;
use App\Models\Data;
use App\Models\Department;
use App\Models\Epass;
use App\Models\SMS;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Knox\AFT\AFT;

class DataController extends Controller
{
    public  function callIPRS(Request $request){
        if(strlen($request->keyword)<1){
            return ['status'=>false,'message'=>'Invalid search key work','trial'=>true];
        }
        $company_id=Auth::user()->company_id;
        $data_count=Data::where('company_id',$company_id)->count();
        $company=Company::find($company_id);
        if($data_count>=5  && $company->enabled=='NO'){
            return ['status'=>false,'message'=>'You have exceeded your trial limit.Please contact '.$company->company_name.' or email us at support@smartguard.net for more information.','trial'=>false];
        }

        $record=Data::where('id_no',$request->keyword)->orWhere('phone',$request->keyword)->orwhere('car_reg_no',$request->keyword)->latest()->first();
        if(empty($record)){
            $status=Blacklist::where('branch_id',Auth::user()->branch_id)->where('id_no',$request->keyword)->count();
        }else{
            $status=Blacklist::where('branch_id',Auth::user()->branch_id)->where('id_no',$request->keyword)->orWhere('id_no',$record->id_no)->count();
        }
        if($status>=1){
            return ['status'=>false,'message'=>'This user is blacklisted from this premise'];
        }

        if(empty($record)){
            if(empty($request->fname) && empty($request->lname)){
                $fname='';
                $lname='';
            }else{
                $fname=$request->fname;
                $lname=$request->lname;
            }
            $data=(array(
                "id_no"=>$request->keyword,
                "fname"=>$fname,
                "lname"=>$lname,
                "kra"=>'',
                "gender"=>'',
                "phone"=>'',
                "dob"=>'',
                "status"=>$status,
            ));
        }else{
            if(empty($request->fname) && empty($request->lname)){
                $fname=$record->fname;
                $lname=$record->lname;
            }else{
                $fname=$request->fname;
                $lname=$request->lname;
            }
            $data=(array(
                "id_no"=>$record->id_no,
                "fname"=>$fname,
                "lname"=>$lname,
                "kra"=>$record->kra,
                "gender"=>$record->gender,
                "phone"=>$record->phone,
                "dob"=>$record->dob,
                "status"=>$status,
            ));
        }

        return ['status'=>true,'data'=>$data,'message'=>'Query successful'];
    }

    public  function searchOut(Request $request){
        $record=Data::where('id_no',$request->keyword)->whereDate('created_at', Carbon::today())->where('status','checkedin')->latest()->first();
        if(empty($record) || $record->branch_id!=Auth::user()->branch_id){
            return ['status'=>false,'message'=>'The client you searched has never checked in today'];
        }
            $status=Blacklist::where('branch_id',Auth::user()->branch_id)->where('id_no',$request->keyword)->orWhere('id_no',$record->id_no)->count();

        return ['status'=>true,'data'=>$record,'access_status'=>$status];
    }

    public  function checkout(Request $request){

        $request['epass_status']='Inactive';
        $e=Epass::where('data_id',$request->id)->first();
        $epass=Epass::find($e->id);
        $epass->update($request->all());

        $data=Data::find($request->id);
        $request['status']='checkedout';
        $request['updated_by']=Auth::user()->id;
        $data->update($request->all());


        $branch=Branch::find(Auth::user()->branch_id);
        $client=Client::where('id',$branch->client_id)->first();
        if($branch->sms=='YES'){

            if(strlen($data->phone)==10){
                $phone=$data->phone;
            }else{
                $phone=str_replace(' ','','0'.substr($data->phone,4));
            }

            try{
                $message='Thank you '.$data->fname.' for visiting  '.$client->name.' '.$branch->name;
                AFT::sendMessage($phone, $messageC,'Postman');
            } catch (\Exception $e) {

            }
            $request['phone']=$phone;
            $request['message']='Hello '.$data->fname.' for visiting'.$client->name.'   '.$branch->name;
            $sms=SMS::create($request->all());
        }
        return ['status'=>true,'message'=>'Information updated successfully'];
    }

    public  function client_save_data(Request $request){
        $request['created_by']=Auth::user()->id;
        $request['client_id']=Auth::user()->client_id;
        $request['branch_id']=Auth::user()->branch_id;
        $request['company_id']=Auth::user()->company_id;
        $request['status']='checkedin';
        $data=Data::create($request->all());
        $branch=Branch::find($request->branch_id);
        $client=Client::where('id',$branch->client_id)->first();

       if(strlen($request->phone)==10){
           $phone=$request->phone;
       }else{
           $phone=str_replace(' ','','0'.substr($request->phone,4));
       }
       if($branch->sms=='YES'){
          $c = substr($client->name, 0, 1);
          $b = substr($branch->name, 0, 1);
          $request['epass']=strtoupper($c.$b.mt_rand(1000,9999));
          $request['data_id']=$data->id;
          $pass=Epass::create($request->all());

           try{
            $message='Hello '.$request->fname.', you checked in  '.$client->name.' , '.$branch->name.'. Your E-PASS  is '.$request['epass'].' Thank you for visiting us.Stay safe.';
            AFT::sendMessage($phone, $message,'Postman');
           } catch (\Exception $e) {

           }

           $request['phone']=$phone;
           $request['message']='Hello '.$request->fname.', you checked in'.$client->name.'  , '.$branch->name.'.Thank you for visiting us.Stay safe.';
           $sms=SMS::create($request->all());
           $dep=Department::find($request->department_id);
           $manager=User::where('branch_id',$branch->id)->first();
           if(!empty($dep) && strlen($dep->phone)>=10){
               if(strlen($dep->phone)==10){
                   $phone=$dep->phone;
               }else{
                   $phone=str_replace(' ','','0'.substr($dep->phone,4));
               }

               try{
                $message='Note that, Visitor  '.$request->epass.',  checked in '.$client->name.' , '.$branch->name.' to see you.Their E-PASS is '.$request['epass'].'.Thank you';
                AFT::sendMessage($phone, $message,'Postman');
               } catch (\Exception $e) {

               }

               $request['phone']=$phone;
               $request['message']='Note that, '.$request->fname.',  checked in '.$client->name.'  '.$branch->name.' to see you.Thank you.';
               $sms=SMS::create($request->all());
           }
           if((int)$request->temperature>=37){
               if(strlen($manager->phone)==10){
                   $phone=$manager->phone;
               }else{
                   $phone=str_replace(' ','','0'.substr($manager->phone,4));
               }

               try{
                $message='Visitor, '.$request->epass.',  checked in '.$client->name.' , '.$branch->name.' with a body temperature of '.$request->temperature.' degrees.';
                AFT::sendMessage($phone, $message,'Postman');
               } catch (\Exception $e) {

               }

               $request['phone']=$phone;
               $request['message']='Note that, '.$request->fname.',  checked in '.$client->name.'  at '.$branch->name.' with a body temperature of '.$request->temperature.' degrees.';
               $sms=SMS::create($request->all());
           }
           if($request->status>0){
               if(strlen($manager->phone)==10){
                   $phone=$manager->phone;
               }else{
                   $phone=str_replace(' ','','0'.substr($manager->phone,4));
               }

               try{
                $message='Note that, visitor '.$request->epass.',  has been allowed in '.$client->name.' at '.$branch->name.' but is blacklisted.';
                AFT::sendMessage($phone, $message,'Postman');
               } catch (\Exception $e) {

               }

               $request['phone']=$phone;
               $request['message']='Note that, '.$request->fname.',  has been allowed in '.$client->name.' at '.$branch->name.' but is blacklist.';
               $sms=SMS::create($request->all());
           }
       }

        return ['status'=>true,'message'=>'Information is saved successfully'];
    }





    public function pushImage(Request $request){
        if ($request->hasFile('imagefile')) {
            return ['status'=>false,'message'=>'Please take an image'];
        }
        $data=['imagefile'=>$request->file('imagefile')];
        $response = Http::withHeaders(['Content-Type'=>'application/json'])->post($request->ocrUrl,$data);
        return dd($response);

//       return ['status'=>true,'message'=>'success','result'=>$];
    }
}
