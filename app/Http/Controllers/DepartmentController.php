<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DepartmentController extends Controller
{
    public  function client_add_department(Request $request){
        $request['created_by']=Auth::user()->id;
        $request['branch_id']=Auth::user()->branch_id;
        $dep=Department::create($request->all());
        return ['status'=>true,'message'=>'Department saved successfully'];
    }

    public  function client_get_department(){
        $dep=Department::where('branch_id',Auth::user()->branch_id)->get();
        return ['department'=>$dep];
    }


    
    public  function client_edit_department(Request $request){
        $dep=Department::find($request->id);
        $dep->update($request->all());
        return ['status'=>true,'message'=>'Department updated successfully'];
    }


}
