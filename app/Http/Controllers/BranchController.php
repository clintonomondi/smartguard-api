<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Knox\AFT\AFT;

class BranchController extends Controller
{
    public  function security_add_branch(Request $request,$id){
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'contact' => 'required',
        ]);
        $request['created_by']=Auth::user()->id;
        $request['updated_by']=Auth::user()->id;
        $request['client_id']=$id;
        $data=Branch::create($request->all());

        if($request->decision===true){
            $code = mt_rand(1000, 9999);
           $request['name']=$request->contact_name;
           $request['client_id']=$id;
           $request['branch_id']=$data->id;
           $request['user_type']='client';
           $request['role']='admin';
           $request['password']=bcrypt($code);
           $user=User::create($request->all());
           $branch=Branch::find($data->id);

            $phone=str_replace(' ','','0'.substr($request->phone,4));
            try{
                $message='Hello '.$request->name.',you have been added on SmartGuard  as the super admin under '.$branch->name.'. Please use email '.$request->email.' and  '.$code.' as your  password. https://smatguards.net';
                AFT::sendMessage($phone, $message,'Postman');
            } catch (\Exception $e) {

            }
        }
        return ['status'=>true,'message'=>'Branch added successfully'];

    }

    public  function security_get_branch($id){
        $branches=Branch::where('client_id',$id)->get();
        return ['branches'=>$branches];
    }

    public  function security_get_branch_detail($id){
        $clent=Branch::find($id);
        $contacts=User::where('branch_id',$id)->where('role','!=','guard')->get();
        return ['branch'=>$clent,'contacts'=>$contacts];
    }

    public  function security_update_branch_detail(Request $request,$id){
    $client=Branch::find($id);
    $request['updated_by']=Auth::user()->id;
    $client->update($request->all());
    return ['status'=>true,'message'=>'Client updated successfully'];
}

public  function branch_get_report(Request $request){
    $id=Auth::user()->branch_id;
    $report=DB::select( DB::raw("SELECT timein,timeout, fname,mname,lname,phone,id_no,temperature,car_reg_no,reason,created_at,
(SELECT NAME FROM `departments` B WHERE B.id=A.department_id)department,
(SELECT name FROM `users` B WHERE B.id=A.created_by)user,
(SELECT COUNT(*) FROM `blacklists` B WHERE B.id_no=A.id_no)blacklist
 FROM `data` A WHERE branch_id='$id' AND  DATE(created_at) BETWEEN '$request->date_from' AND '$request->date_to' ORDER BY id DESC LIMIT 500") );
    return ['report'=>$report];
}

public  function security_add_contact(Request $request,$id){
        $branch=Branch::find($id);
    $code = mt_rand(1000, 9999);
    $request['client_id']=$branch->client_id;
    $request['branch_id']=$id;
    $request['user_type']='client';
    $request['role']='admin';
    $request['password']=bcrypt($code);
    $user=User::create($request->all());

    $phone=str_replace(' ','','0'.substr($request->phone,4));

    try{
        $message='Hello '.$request->name.',you have been added on SmartGuard  as the super admin under '.$branch->name.'. Please use email '.$request->email.' and  '.$code.' as your  password. https://smatguards.net';
        AFT::sendMessage($phone, $message,'Postman');
    } catch (\Exception $e) {

    }
    return ['status'=>true,'message'=>'contact added successfully'];
}

public  function admin_get_branches(Request $request){
   $branches=Branch::where('client_id',$request->client_id)->get();
   return ['branches'=>$branches];
}
}
