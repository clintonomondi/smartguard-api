<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{

    
    public  function security_get_report(Request $request){
        if(empty($request->guard_id)) {
            $report = DB::select(DB::raw("SELECT timein,timeout,fname,mname,lname,phone,id_no,temperature,car_reg_no,reason,created_at,laptop,
(SELECT name FROM `departments` B WHERE B.id=A.department_id)department,
(SELECT name FROM `users` B WHERE B.id=A.created_by)user
 FROM `data` A WHERE branch_id='$request->branch_id' AND  DATE(created_at) BETWEEN '$request->date_from' AND '$request->date_to' ORDER BY id DESC LIMIT 500"));
        }else{
            $report = DB::select(DB::raw("SELECT timein,timeout, fname,mname,lname,phone,id_no,temperature,car_reg_no,reason,created_at,laptop,
(SELECT name FROM `departments` B WHERE B.id=A.department_id)department,
(SELECT name FROM `users` B WHERE B.id=A.created_by)user
 FROM `data` A WHERE created_by='$request->guard_id' AND  DATE(created_at) BETWEEN '$request->date_from' AND '$request->date_to' ORDER BY id DESC LIMIT 500"));
        }
        return ['report'=>$report];
    }
    public  function admin_get_report(Request $request){
        if(empty($request->guard_id)) {
            $report = DB::select(DB::raw("SELECT  timein,timeout,fname,mname,lname,phone,id_no,temperature,car_reg_no,reason,created_at,laptop,
(SELECT name FROM `departments` B WHERE B.id=A.department_id)department,
(SELECT name FROM `users` B WHERE B.id=A.created_by)user
 FROM `data` A WHERE branch_id='$request->branch_id' AND  DATE(created_at) BETWEEN '$request->date_from' AND '$request->date_to' ORDER BY id DESC LIMIT 500"));
        }else{
            $report = DB::select(DB::raw("SELECT timein,timeout, fname,mname,lname,phone,id_no,temperature,car_reg_no,reason,created_at,laptop,
(SELECT name FROM `departments` B WHERE B.id=A.department_id)department,
(SELECT name FROM `users` B WHERE B.id=A.created_by)user
 FROM `data` A WHERE created_by='$request->guard_id' AND  DATE(created_at) BETWEEN '$request->date_from' AND '$request->date_to' ORDER BY id DESC LIMIT 500"));
        }
        return ['report'=>$report];
    }

    public  function branch_get_report_by_guard(Request $request){
        $report = DB::select(DB::raw("SELECT timein,timeout, fname,mname,lname,phone,id_no,temperature,car_reg_no,reason,created_at,laptop,
(SELECT name FROM `departments` B WHERE B.id=A.department_id)department,
(SELECT name FROM `users` B WHERE B.id=A.created_by)user,
(SELECT COUNT(*) FROM `blacklists` B WHERE B.id_no=A.id_no)blacklist
 FROM `data` A WHERE created_by='$request->guard_id' AND  DATE(created_at) BETWEEN '$request->date_from' AND '$request->date_to' ORDER BY id DESC LIMIT 500"));

     return ['report'=>$report];
    }


    public  function branch_get_report_by_dep(Request $request){
        $report = DB::select(DB::raw("SELECT timein,timeout, fname,mname,lname,phone,id_no,temperature,car_reg_no,reason,created_at,laptop,
(SELECT name FROM `departments` B WHERE B.id=A.department_id)department,
(SELECT name FROM `users` B WHERE B.id=A.created_by)user,
(SELECT COUNT(*) FROM `blacklists` B WHERE B.id_no=A.id_no)blacklist
 FROM `data` A WHERE department_id='$request->dep_id' AND  DATE(created_at) BETWEEN '$request->date_from' AND '$request->date_to' ORDER BY id DESC LIMIT 500"));

        return ['report'=>$report];
    }

    public function branch_get_report_by_frequent(Request $request){
        $branch_id=Auth::user()->branch_id;
        $user = DB::select(DB::raw("SELECT id_no FROM data WHERE branch_id='$branch_id' GROUP BY id_no ORDER BY COUNT(*) DESC LIMIT 1"));
        $id_no=$user[0]->id_no;

        $report = DB::select(DB::raw("SELECT timein,timeout, fname,mname,lname,phone,id_no,temperature,car_reg_no,reason,created_at,laptop,
(SELECT name FROM `departments` B WHERE B.id=A.department_id)department,
(SELECT name FROM `users` B WHERE B.id=A.created_by)user,
(SELECT COUNT(*) FROM `blacklists` B WHERE B.id_no=A.id_no)blacklist
 FROM `data` A WHERE  branch_id='$branch_id' AND id_no='$id_no' ORDER BY id DESC LIMIT 500"));
        return ['report'=>$report];
}

public  function branch_get_report_by_rand(Request $request){
    $branch_id=Auth::user()->branch_id;
    $key=$request->key_word;
    $report = DB::select(DB::raw("SELECT *,
(SELECT COUNT(*) FROM `blacklists` B WHERE B.id_no=A.id_no)blacklist
 FROM data A WHERE fname LIKE '$key%' OR  lname LIKE '$key%' OR id_no LIKE '$key%' OR phone LIKE '$key%' AND branch_id='$branch_id' ORDER BY id DESC LIMIT 500"));
    return ['report'=>$report];
}

public function branch_get_report_by_range(Request $request){
    $branch_id=Auth::user()->branch_id;
    $min=(int)$request->min;
    $max=(int)$request->max;

    $report = DB::select(DB::raw("SELECT timein,timeout, fname,mname,lname,phone,id_no,temperature,car_reg_no,reason,created_at,laptop,
(SELECT name FROM `departments` B WHERE B.id=A.department_id)department,
(SELECT name FROM `users` B WHERE B.id=A.created_by)user,
(SELECT COUNT(*) FROM `blacklists` B WHERE B.id_no=A.id_no)blacklist
 FROM `data` A WHERE branch_id='$branch_id' AND  temperature BETWEEN '$min' AND '$max' ORDER BY id DESC LIMIT 500"));

    return ['report'=>$report];
}
}
