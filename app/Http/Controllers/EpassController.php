<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Client;
use App\Models\Data;
use App\Models\Epass;
use App\Models\SMS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class EpassController extends Controller
{
    public  function client_get_epass(){
        $branch_id=Auth::user()->branch_id;
        $epass = DB::select( DB::raw("SELECT *,
(SELECT timein FROM data B WHERE B.id=A.data_id)timein
 FROM epasses A WHERE branch_id='$branch_id' AND epass_status='Active' ORDER BY id DESC") );
        return ['epass'=>$epass];
    }
    public  function client_get_epass_ontype(Request $request){
        $branch_id=Auth::user()->branch_id;
        $key=$request->keyword;
        if(empty($key)){
            $epass=Epass::orderBy('id','desc')->where('branch_id',Auth::user()->branch_id)->where('epass_status','Active')->get();
        }else{
            $epass = DB::select( DB::raw("SELECT * FROM epasses WHERE branch_id='$branch_id' AND epass_status='Active' AND  epass LIKE '$key%' OR id_no LIKE '$key%' ") );
        }

        return ['epass'=>$epass];
    }
    public  function client_get_epass_info(Request $request){
        $data=Data::find($request->data_id);
        $epass_data=Epass::find($request->id);
        return ['data'=>$data,'epaass_data'=>$epass_data,'status'=>true];
    }


    
    public  function client_get_epass_checkout(Request $request){
        $request['status']='checkedout';
        $request['epass_status']='Inactive';
        $request['served_by']=Auth::user()->id;
        $request['updated_by']=Auth::user()->id;
        $epass=Epass::find($request->id);
        $epass->update($request->all());

        return ['status'=>true,'message'=>'Information updated successfully'];
    }
}
