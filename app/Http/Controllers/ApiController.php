<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Client;
use App\Models\Company;
use App\Models\SMS;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{

    public  function  security_get_data(){
        $company_id=Auth::user()->company_id;
        $year=date("Y");
        $clients=Client::where('company_id',Auth::user()->company_id)->count();
        $guards=User::where('company_id',Auth::user()->company_id)->where('role','guard')->count();
        $records=DB::select( DB::raw("SELECT count(*)records FROM `data` WHERE  created_at>=CURDATE()") );

        $data=DB::select( DB::raw("SELECT 
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='1' AND YEAR(created_at)='$year' AND company_id='$company_id')Jan,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='2' AND YEAR(created_at)='$year' AND company_id='$company_id')Feb,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='3' AND YEAR(created_at)='$year' AND company_id='$company_id')Mar,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='4' AND YEAR(created_at)='$year' AND company_id='$company_id')Apr,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='5' AND YEAR(created_at)='$year' AND company_id='$company_id')May,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='6' AND YEAR(created_at)='$year' AND company_id='$company_id')Jun,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='7' AND YEAR(created_at)='$year' AND company_id='$company_id')Jul,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='8' AND YEAR(created_at)='$year' AND company_id='$company_id')Aug,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='9' AND YEAR(created_at)='$year' AND company_id='$company_id')Sept,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='10' AND YEAR(created_at)='$year' AND company_id='$company_id')Oct,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='11' AND YEAR(created_at)='$year' AND company_id='$company_id')Nov,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='12' AND YEAR(created_at)='$year' AND company_id='$company_id')Dece
 FROM DUAL ") );
        $data2=DB::select( DB::raw("SELECT  WEEK(created_at)AS week,
(SELECT COUNT(*) FROM data B WHERE WEEK(created_at)=week)visitor
  FROM data A WHERE company_id='$company_id' GROUP BY week ORDER BY week DESC  LIMIT 4") );
        $data3=DB::select( DB::raw("SELECT *,
(SELECT COUNT(*) FROM branches B WHERE B.client_id IN (SELECT id FROM clients B WHERE B.company_id=A.id))branches,
(SELECT COUNT(*) FROM s_m_s B WHERE STATUS='Unpaid' AND B.client_id IN (SELECT id FROM clients B WHERE B.company_id=A.id))sms
 FROM `companies` A WHERE id='$company_id'") );


        return ['data'=>$data,'data2'=>$data2,'clients'=>$clients,'guards'=>$guards,'records'=>$records,'data3'=>$data3[0]];
    }

    public  function client_get_data(){
        $branch_id=Auth::user()->branch_id;
        $year=date("Y");
        $guards=User::where('client_id',Auth::user()->client_id)->where('role','guard')->count();
        $records=DB::select( DB::raw("SELECT count(*)records FROM `data` WHERE  created_at>=CURDATE()") );

        $data=DB::select( DB::raw("SELECT 
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='1' AND YEAR(created_at)='$year' AND branch_id='$branch_id')Jan,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='2' AND YEAR(created_at)='$year' AND branch_id='$branch_id')Feb,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='3' AND YEAR(created_at)='$year' AND branch_id='$branch_id')Mar,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='4' AND YEAR(created_at)='$year' AND branch_id='$branch_id')Apr,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='5' AND YEAR(created_at)='$year' AND branch_id='$branch_id')May,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='6' AND YEAR(created_at)='$year' AND branch_id='$branch_id')Jun,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='7' AND YEAR(created_at)='$year' AND branch_id='$branch_id')Jul,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='8' AND YEAR(created_at)='$year' AND branch_id='$branch_id')Aug,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='9' AND YEAR(created_at)='$year' AND branch_id='$branch_id')Sept,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='10' AND YEAR(created_at)='$year' AND branch_id='$branch_id')Oct,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='11' AND YEAR(created_at)='$year' AND branch_id='$branch_id')Nov,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='12' AND YEAR(created_at)='$year' AND branch_id='$branch_id')Dece
 FROM DUAL ") );
        $data2=DB::select( DB::raw("SELECT  WEEK(created_at)AS week,
(SELECT COUNT(*) FROM data B WHERE WEEK(created_at)=week)visitor
  FROM data A WHERE branch_id='$branch_id'  GROUP BY week ORDER BY week DESC  LIMIT 4") );

        $data4=DB::select( DB::raw("SELECT department_id,
(SELECT IF(NAME IS NULL,'',NAME) FROM departments B WHERE B.id=A.department_id)department,
(SELECT COUNT(*) FROM `data` B WHERE B.department_id=A.department_id)visitors
 FROM `data` A  WHERE branch_id='$branch_id' AND YEAR(created_at)='$year' GROUP BY department_id") );

        return ['data'=>$data,'data2'=>$data2,'guards'=>$guards,'records'=>$records,'data4'=>$data4];
    }

    public  function guard_get_data(){
        $id=Auth::user()->id;
        $year=date("Y");
        $branch_id=Auth::user()->branch_id;
        $data=DB::select( DB::raw("SELECT 
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='1' AND YEAR(created_at)='$year' AND created_by='$id')Jan,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='2' AND YEAR(created_at)='$year' AND created_by='$id')Feb,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='3' AND YEAR(created_at)='$year' AND created_by='$id')Mar,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='4' AND YEAR(created_at)='$year' AND created_by='$id')Apr,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='5' AND YEAR(created_at)='$year' AND created_by='$id')May,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='6' AND YEAR(created_at)='$year' AND created_by='$id')Jun,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='7' AND YEAR(created_at)='$year' AND created_by='$id')Jul,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='8' AND YEAR(created_at)='$year' AND created_by='$id')Aug,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='9' AND YEAR(created_at)='$year' AND created_by='$id')Sept,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='10' AND YEAR(created_at)='$year' AND created_by='$id')Oct,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='11' AND YEAR(created_at)='$year' AND created_by='$id')Nov,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='12' AND YEAR(created_at)='$year' AND created_by='$id')Dece
 FROM DUAL ") );
        $data2=DB::select( DB::raw("SELECT  WEEK(created_at)AS week,
(SELECT  COUNT(*) FROM data B WHERE WEEK(created_at)=week)visitor
  FROM data A WHERE created_by='$id'  GROUP BY week ORDER BY week DESC  LIMIT 4") );

        $data4=DB::select( DB::raw("SELECT department_id,
(SELECT IF(NAME IS NULL,'',NAME) FROM departments B WHERE B.id=A.department_id)department,
(SELECT COUNT(*) FROM `data` B WHERE B.department_id=A.department_id)visitors
 FROM `data` A WHERE branch_id='$branch_id' AND YEAR(created_at)='$year' GROUP BY department_id") );

        return ['data'=>$data,'data2'=>$data2,'data4'=>$data4];
    }

    public  function admin_get_data(){
        $year=date("Y");
        $data=DB::select( DB::raw("SELECT 
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='1' AND YEAR(created_at)='$year' )Jan,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='2' AND YEAR(created_at)='$year' )Feb,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='3' AND YEAR(created_at)='$year' )Mar,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='4' AND YEAR(created_at)='$year' )Apr,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='5' AND YEAR(created_at)='$year' )May,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='6' AND YEAR(created_at)='$year' )Jun,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='7' AND YEAR(created_at)='$year' )Jul,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='8' AND YEAR(created_at)='$year' )Aug,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='9' AND YEAR(created_at)='$year' )Sept,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='10' AND YEAR(created_at)='$year' )Oct,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='11' AND YEAR(created_at)='$year' )Nov,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM data WHERE MONTH(created_at)='12' AND YEAR(created_at)='$year')Dece
 FROM DUAL ") );
        $data2=DB::select( DB::raw("SELECT  WEEK(created_at)AS week,
(SELECT COUNT(*) FROM data B WHERE WEEK(created_at)=week)visitor
  FROM data A   GROUP BY week ORDER BY week DESC  LIMIT 4") );
        $record = DB::select( DB::raw("SELECT count(*)records FROM `data` WHERE  created_at>=CURDATE()") );

        $companies=Company::count();
        $clients=Client::count();
        $branches=Branch::count();
        $guards=User::where('role','guard')->count();
        $sms=SMS::where('status','Unpaid')->count();

        return ['sms'=>$sms,'data'=>$data,'data2'=>$data2,'companies'=>$companies,'branches'=>$branches,'clients'=>$clients,'guards'=>$guards,'records'=>$record];
    }
}
