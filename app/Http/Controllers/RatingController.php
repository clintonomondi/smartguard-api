<?php

namespace App\Http\Controllers;

use App\Models\Blacklist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RatingController extends Controller
{
    public  function blacklist(Request $request){
        $check=Blacklist::where('id_no',$request->id_no)->where('branch_id',Auth::user()->branch_id)->count();
        if($check>0){
            return ['status'=>false,'message'=>'The client with with ID number has been blacklisted in this branch'];
        }
        $request['created_by']=Auth::user()->id;
        $request['branch_id']=Auth::user()->branch_id;
        $request['client_id']=Auth::user()->client_id;
        $request['company_id']=Auth::user()->company_id;
        $data=Blacklist::create($request->all());
        return ['status'=>true,'message'=>'Client blicklisted successfully'];
    }

    public  function client_get_blacklist(){
        $blacklist=Blacklist::where('branch_id',Auth::user()->branch_id)->get();
        return ['blacklist'=>$blacklist];
    }


    
    public  function unlock(Request $request){
        $data=Blacklist::find($request->id);
        $data->delete();
        return ['status'=>true,'message'=>'Client unlocked successfully'];
    }
}
