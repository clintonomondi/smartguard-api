<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Knox\AFT\AFT;

class ClientController extends Controller
{
    public  function security_add_client(Request $request){
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'contact' => 'required',
            'country' => 'required',
            'region' => 'required',
        ]);
        $request['created_by']=Auth::user()->id;
        $request['updated_by']=Auth::user()->id;
        $request['company_id']=Auth::user()->company_id;
        $data=Client::create($request->all());
        return ['status'=>true,'message'=>'Client added successfully'];

    }

    public  function security_get_client(){
        $clients=Client::where('company_id',Auth::user()->company_id)->get();
        return ['clients'=>$clients];
    }

    public  function security_get_client_detail($id){
        $clent=Client::find($id);
        return ['client'=>$clent];
    }
    public  function security_update_client_detail(Request $request,$id){
        $client=Client::find($id);
        $request['updated_by']=Auth::user()->id;
        $client->update($request->all());
        return ['status'=>true,'message'=>'Client updated successfully'];
    }

    public  function admin_get_clients(Request $request){
        $clients = DB::select( DB::raw("SELECT *,
(SELECT COUNT(*)vcount FROM branches B WHERE B.client_id=A.id)branches
 FROM `clients` A WHERE company_id='$request->id'") );

        $guards=DB::select( DB::raw("SELECT *,
(SELECT NAME FROM branches B WHERE B.id=A.branch_id)branch
 FROM `users` A WHERE role='guard' AND company_id='$request->id'") );
        return ['clients'=>$clients,'guards'=>$guards];
    }
}
