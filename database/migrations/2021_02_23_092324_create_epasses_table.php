<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEpassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('epasses', function (Blueprint $table) {
            $table->id();
            $table->string('epass');
            $table->string('data_id');
            $table->string('id_no')->nullable();
            $table->string('branch_id')->nullable();
            $table->string('served_by')->nullable();
            $table->string('epass_status')->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('epasses');
    }
}
